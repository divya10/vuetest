import Vue from "vue";
import VueRouter from "vue-router";
import Register from "../views/Register.vue";
//import Login from "../views/login.vue";
//import Secure from "../views/Secure.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Register",
    component: Register
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import("../views/Login.vue")
  },
  {
    path: "/secure",
    name: "Secure",
    component: () =>
      import("../views/Secure.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
